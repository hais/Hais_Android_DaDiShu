package pw.hais.dadishu.entitys;

import com.avos.avoscloud.AVObject;

/**
 * 用户信息
 * Created by Administrator on 2015/7/10.
 */
public class User{
    public String name;     //昵称
    public String time;         //时间
    public long nowResults;       //分数
    public long nowScenarion;       //当前卡关

    public User(String name, String time, long nowResults, long nowScenarion) {
        this.name = name;
        this.time = time;
        this.nowResults = nowResults;
        this.nowScenarion = nowScenarion;
    }
}
