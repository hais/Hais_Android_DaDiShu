package pw.hais.dadishu.entitys;

import java.io.Serializable;

/**
 * 游戏场景信息
 * Created by HaiSheng on 2015/7/4.
 */
public class GameScenarioInfo implements Serializable{
    public int nowScenarion;    //当前关卡
    public int nowResults;     //当前分数
    public int nowlife;         //当前生命
    //public int nowOverplusDiShu;        //当前剩余地鼠数量
    public int nowWinGameNum;        //当前胜利剩余地鼠数量

    public int scenarioDiSuStopTime;   //地鼠停止时间
    public int scenarioOneSecUpNum;  //出现地鼠的间隔
    public int scenarioDiShuOutNum;  //地鼠死亡获得的分数
    //public int scenarioDiSuCountNum;   //地鼠总数量
    public int scenarioWinGameNum;  //胜利数量
    public int scenarioBackground;  //当前游戏背景图片


}
