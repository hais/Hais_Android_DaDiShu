package pw.hais.dadishu.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import pw.hais.dadishu.R;
import pw.hais.utils.ViewHolderUtils;

/**
 * Created by HaiSheng on 2015/7/4.
 */
public class GameLoveAdapter extends BaseAdapter{
    private ViewHolder mHolder;
    private int loveNum;    //生命数量

    public GameLoveAdapter(int loveNum){
        this.loveNum = loveNum;
    }

    public void setLoveNum(int loveNum){
      this.loveNum = loveNum;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return loveNum;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        mHolder = new ViewHolder();
        view = ViewHolderUtils.loadingConvertView(parent.getContext(), view, R.layout.activity_lpaygame_love_item, ViewHolder.class);
        return view;
    }

    public static class ViewHolder {
        public ImageView image_btn_love;
    }

}
