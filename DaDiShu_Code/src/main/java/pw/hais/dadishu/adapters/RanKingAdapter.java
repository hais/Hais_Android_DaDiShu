package pw.hais.dadishu.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pw.hais.dadishu.R;
import pw.hais.dadishu.entitys.User;
import pw.hais.utils.ViewHolderUtils;

/**
 * 排名
 * Created by HaiSheng on 2015/7/4.
 */
public class RanKingAdapter extends BaseAdapter{
    private ViewHolder mHolder;
    private List<User> list;

    public RanKingAdapter(List<User> list){
        this.list = list;
    }

    public void setList(List<User> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        mHolder = new ViewHolder();

        if(view == null)
        {
            view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_ranking_item,null);
            mHolder = new ViewHolder();
            mHolder.text_sort = (TextView) view.findViewById(R.id.text_sort);
            mHolder.text_scenarion = (TextView) view.findViewById(R.id.text_scenarion);
            mHolder.text_results = (TextView) view.findViewById(R.id.text_results);
            mHolder.text_name = (TextView) view.findViewById(R.id.text_name);
            view.setTag(mHolder);
        }else
        {
            mHolder = (ViewHolder) view.getTag();
        }

        mHolder.text_sort.setText((i+1)+"");
        mHolder.text_scenarion.setText(""+list.get(i).nowScenarion);
        mHolder.text_results.setText(""+list.get(i).nowResults);
        mHolder.text_name.setText(""+list.get(i).name);

        return view;
    }

    public static class ViewHolder {
        public TextView text_sort;
        public TextView text_scenarion;
        public TextView text_results;
        public TextView text_name;
    }

}
