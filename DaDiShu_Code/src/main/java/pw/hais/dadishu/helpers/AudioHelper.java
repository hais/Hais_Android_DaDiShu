package pw.hais.dadishu.helpers;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import java.util.HashMap;

import pw.hais.dadishu.R;


/**
 * 音乐播放助手
 * Created by HaiSheng on 2015/6/28.
 */
public class AudioHelper {
    public static HashMap<Integer, Integer> musicMap;
    public static SoundPool musicSP;

    public static int DI_SHU_BG = 1;
    public static int DI_SHU_UP = 2;
    public static int DI_SHU_CLICK = 3;
    public static int DI_SHU_PAO = 4;
    public static int DI_SHU_WIN = 5;
    public static int DI_SHU_FAILED = 6;
    public static int BOMB = 7;

    public static void initSound(Context context) {
        musicSP = new SoundPool(5, AudioManager.STREAM_MUSIC, 100);
        musicMap = new HashMap<Integer, Integer>();
        musicMap.put(DI_SHU_BG,musicSP.load(context, R.raw.bg,1));
        musicMap.put(DI_SHU_UP,musicSP.load(context,R.raw.dishu_up,1));
        musicMap.put(DI_SHU_CLICK,musicSP.load(context,R.raw.dishu_click,1));
        musicMap.put(DI_SHU_PAO,musicSP.load(context,R.raw.dishu_pao,1));
        musicMap.put(DI_SHU_WIN,musicSP.load(context,R.raw.win,1));
        musicMap.put(DI_SHU_FAILED,musicSP.load(context,R.raw.failed,1));
        musicMap.put(BOMB,musicSP.load(context,R.raw.bomb,1));
    }

    /**
     * 开始播放
     * @param sound  声音
     * @param number    次数
     */
    public static void playSound(int sound, int number) {
        musicSP.play(musicMap.get(sound),1,1,5,number,1);
    }

    /**
     * 停止播放
     * @param sound
     */
    public static void pauseSound(int sound){
        musicSP.pause(sound);
        musicSP.autoPause();
        musicSP.stop(sound);
    }

}
