package pw.hais.dadishu.ui;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import pw.hais.dadishu.R;
import pw.hais.ui.BasePopupWindow;
import pw.hais.utils.L;

/**
 * 游戏失败
 * Created by Administrator on 2015/7/10.
 */
public abstract class GameFailedPopupWindow extends BasePopupWindow{
    private TextView text_msg;
    private Button btn_return,btn_ranking;


    public GameFailedPopupWindow(Activity activity) {
        setLayout(activity,R.layout.game_failed_popup_windows);
    }

    public void setText_msg(String msg){
        text_msg.setText(msg);
    }

    @Override
    protected void drawEnd(View popupWindowView) {
        text_msg = (TextView) popupWindowView.findViewById(R.id.text_msg);
        btn_return = (Button) popupWindowView.findViewById(R.id.btn_return);
        btn_ranking = (Button) popupWindowView.findViewById(R.id.btn_ranking);

        btn_return.setOnClickListener(this);


        btn_ranking.setOnClickListener(this);

    }
}
