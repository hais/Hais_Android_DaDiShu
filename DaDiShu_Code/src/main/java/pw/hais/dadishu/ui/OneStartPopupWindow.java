package pw.hais.dadishu.ui;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.avos.avoscloud.AVCloudQueryResult;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.CloudQueryCallback;
import com.avos.avoscloud.FindCallback;

import java.util.List;

import pw.hais.dadishu.R;
import pw.hais.dadishu.entitys.User;
import pw.hais.ui.BasePopupWindow;
import pw.hais.ui.LoadProgressDialog;
import pw.hais.utils.L;
import pw.hais.utils.SPUtils;

/**
 * 游戏失败
 * Created by Administrator on 2015/7/10.
 */
public class OneStartPopupWindow extends BasePopupWindow implements View.OnClickListener{
    private EditText edit_username;
    private Button btn_start;
    private LoadProgressDialog loadProgressDialog;


    public OneStartPopupWindow(Activity activity) {
        setLayout(activity,R.layout.one_start_popup_windows);
    }

    @Override
    protected void drawEnd(View popupWindowView) {
        edit_username = (EditText) popupWindowView.findViewById(R.id.edit_username);
        btn_start = (Button) popupWindowView.findViewById(R.id.btn_start);

        btn_start.setOnClickListener(this);
        loadProgressDialog = new LoadProgressDialog(activity);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_start:
                String text = edit_username.getText() == null ? "" : edit_username.getText().toString();
                if(text.length()==0){
                    L.showShort("请输入 昵称，用于提交游戏分数排名。");
                }else{
                    loadProgressDialog.show("正在同步服务器获取信息。。。");
                    String name = edit_username.getText().toString().trim();
                    AVQuery<AVObject> query = new AVQuery<AVObject>("Results");
                    query.whereEqualTo("name", name);
                    query.findInBackground(new FindCallback<AVObject>() {
                        public void done(List<AVObject> avObjects, AVException e) {
                            loadProgressDialog.dismiss();
                            if (e == null) {
                                L.d("成功", "查询到" + avObjects.size() + " 条符合条件的数据");
                                if (avObjects.size() < 0) {   //没玩过
                                    SPUtils.saveObject("user.name", edit_username.getText().toString().trim());
                                } else {    //玩过
                                    SPUtils.saveObject("user.name", edit_username.getText().toString().trim());
                                    SPUtils.saveObject("user.nowResults", avObjects.get(0).getLong("nowResults"));
                                    SPUtils.saveObject("user.nowScenarion", avObjects.get(0).getLong("nowScenarion"));
                                }
                            } else {
                                L.d("失败", "查询错误: " + e.getMessage());
                            }
                            dismiss();
                        }
                    });
                }
                break;
        }
    }
}
