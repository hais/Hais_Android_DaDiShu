package pw.hais.dadishu.activitys;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import pw.hais.dadishu.R;
import pw.hais.dadishu.app.BaseActivity;

/**
 * 关于 我们
 * Created by HaiSheng on 2015/7/11.
 */
public class AuthorActivity extends BaseActivity implements View.OnClickListener{
    private TextView text_msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author);


        findViewAndSetOnClick(R.id.image_btn_return);

        text_msg = findView(R.id.text_msg);

        text_msg.setText("Hello_海生\nhttp://app.hais.pw\n素材来源于互联网如有侵权请联系作者。");

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.image_btn_return:
                finish();
                break;
        }


        super.onClick(view);
    }
}
