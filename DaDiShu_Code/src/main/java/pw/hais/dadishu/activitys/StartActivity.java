package pw.hais.dadishu.activitys;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.avos.avoscloud.AVAnalytics;
import com.avos.avoscloud.AVOnlineConfigureListener;

import org.json.JSONObject;

import pw.hais.dadishu.R;
import pw.hais.dadishu.app.BaseActivity;
import pw.hais.dadishu.entitys.GameScenarioInfo;
import pw.hais.dadishu.helpers.AudioHelper;
import pw.hais.dadishu.ui.OneStartPopupWindow;
import pw.hais.utils.ApkInfoUtil;
import pw.hais.utils.AppInfoUtil;
import pw.hais.utils.L;
import pw.hais.utils.SPUtils;
import pw.hais.utils.ViewHolderUtils;
import pw.hais.utils.ViewUtil;

/**
 * 启动界面
 * Created by HaiSheng on 2015/7/4.
 */
public class StartActivity extends BaseActivity implements View.OnClickListener {
    public static final String SP_SHENGYIN_KEY = "SP_SHENGYIN_KEY"; //声音是否打开
    private GameScenarioInfo gameScenarioInfo;      //游戏信息
    private boolean hasFocus = false;   //是否界面绘画完成
    private boolean isCheckUpdate = true;
    private ImageView image_btn_shengyin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        findViewAndSetOnClick(R.id.image_btn_author);
        findViewAndSetOnClick(R.id.btn_ranking);
        findViewAndSetOnClick(R.id.btn_start);
        image_btn_shengyin = findViewAndSetOnClick(R.id.image_btn_shengyin);
        //设置声音
        boolean shengyin = SPUtils.getObject(SP_SHENGYIN_KEY, Boolean.class);
        shengyinKongZhi(shengyin);


        //检查版本更新
        AVAnalytics.setOnlineConfigureListener(new AVOnlineConfigureListener() {
            @Override
            public void onDataReceived(JSONObject data) {
                String APP_Version = AVAnalytics.getConfigParams(context, "APP_Version");   //获取服务器版本
                String Now_APP_Version = ApkInfoUtil.getVersionName();  //获取当前版本
                String APP_Version_DOWN_PATH = AVAnalytics.getConfigParams(context, "APP_" + APP_Version + "_PATH");    //获取软件下载地址
                L.i(tag, "111-APP_Version" + APP_Version);
                L.i(tag, "111-Now_APP_Version" + Now_APP_Version);
                L.i(tag, "111-APP_Version_DOWN_PATH" + APP_Version_DOWN_PATH);
                if ((!Now_APP_Version.equals(APP_Version))&& isCheckUpdate) {
                    Intent it = new Intent(Intent.ACTION_VIEW, Uri.parse(APP_Version_DOWN_PATH));
                    it.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                    context.startActivity(it);
                }
                isCheckUpdate = false;
            }
        });

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        if (hasFocus)
        {
            this.hasFocus = hasFocus;
            onResume();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //判断是否可以继续游戏
        gameScenarioInfo = SPUtils.getObject(PlayNormalGameActivity.SP_GAME_Normal_SCENARION_INFO_KEY, GameScenarioInfo.class, null);
        if (gameScenarioInfo != null) {
            findViewAndSetOnClick(R.id.btn_continue).setVisibility(View.VISIBLE);
        }else{
            findViewAndSetOnClick(R.id.btn_continue).setVisibility(View.GONE);
        }

        //判断是否设置了昵称。
        if(hasFocus && SPUtils.getObject("user.name",String.class,null)==null){
            new OneStartPopupWindow(this).showCenter();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start:    //启动新 的正常 游戏
                SPUtils.delObject(PlayNormalGameActivity.SP_GAME_Normal_SCENARION_INFO_KEY);
                startActivity(new Intent(context, PlayNormalGameActivity.class));
                break;
            case R.id.image_btn_shengyin:   //声音控制
                boolean shengyin = SPUtils.getObject(SP_SHENGYIN_KEY, Boolean.class);
                shengyinKongZhi(!shengyin);
                break;
            case R.id.btn_continue: //继续游戏
                startActivity(new Intent(context, PlayNormalGameActivity.class));
                break;
            case R.id.image_btn_author: //关于我们
                startActivity(new Intent(context, AuthorActivity.class));
                break;
            case R.id.btn_ranking:      //排行榜
                startActivity(new Intent(context, RankingActivity.class));
                break;
        }
    }

    /**
     * 是否打开声音
     */
    private void shengyinKongZhi(boolean isON) {
        if (!isON) {   //关闭声音
            image_btn_shengyin.setImageResource(R.drawable.shengyin_off);
            AudioHelper.pauseSound(AudioHelper.DI_SHU_BG);
            SPUtils.saveObject(SP_SHENGYIN_KEY, false);
        } else {  //开启声音
            image_btn_shengyin.setImageResource(R.drawable.shengyin_on);
            AudioHelper.pauseSound(AudioHelper.DI_SHU_BG);
            AudioHelper.playSound(AudioHelper.DI_SHU_BG, 1000);
            SPUtils.saveObject(SP_SHENGYIN_KEY, true);
        }
    }


    private long outTime=0;	//用于 两次退出键，退出软件
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        // 监听菜单键
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                long nowTime = System.currentTimeMillis() / 1000;
                if(nowTime - outTime <= 2 ){
                    finish();
                    app.exitApp();
                }else{
                    L.showShort("再次点击，退出游戏！");
                    outTime = nowTime;
                }
                return true;
        }
        return super.onKeyUp(keyCode, event);
    }

}
