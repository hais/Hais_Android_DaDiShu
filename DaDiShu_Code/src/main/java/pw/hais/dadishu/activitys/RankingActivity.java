package pw.hais.dadishu.activitys;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.avos.avoscloud.AVCloudQueryResult;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.CloudQueryCallback;

import java.util.ArrayList;
import java.util.List;

import pw.hais.dadishu.R;
import pw.hais.dadishu.adapters.RanKingAdapter;
import pw.hais.dadishu.app.BaseActivity;
import pw.hais.dadishu.entitys.User;
import pw.hais.utils.L;

/**
 * 排行榜
 * Created by Administrator on 2015/7/10.
 */
public class RankingActivity extends BaseActivity{
    private ListView listView_ranking;
    private RanKingAdapter ranKingAdapter;
    private List<User> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        listView_ranking = findView(R.id.listView_ranking);
        findViewAndSetOnClick(R.id.image_btn_return);

        list = new ArrayList<>();
        ranKingAdapter = new RanKingAdapter(list);
        listView_ranking.setAdapter(ranKingAdapter);

        loadDialog.show("数据加载中,...");
        AVQuery.doCloudQueryInBackground("select * from Results order by nowResults desc limit 20", new CloudQueryCallback<AVCloudQueryResult>() {
            @Override
            public void done(AVCloudQueryResult result, AVException cqlException) {
                L.i(tag,gson.toJson(result));
                if (cqlException == null) {
                    for (int i=0;i<result.getResults().size();i++){
                        AVObject av = result.getResults().get(i);
                        L.i(tag, gson.toJson(av) + "");
                        list.add(new User(av.getString("name"), "-", av.getLong("nowResults"), av.getLong("nowScenarion")));
                    }
                    L.i(tag,list.size()+"");
                    ranKingAdapter.setList(list);
                    loadDialog.dismiss();

                }
            }
        });



    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.image_btn_return:
                finish();
                break;
        }
        super.onClick(view);
    }
}
