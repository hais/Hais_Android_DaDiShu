package pw.hais.dadishu.dao;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.SaveCallback;

import java.util.List;

import pw.hais.dadishu.entitys.GameScenarioInfo;
import pw.hais.utils.SPUtils;

/**
 * 排行榜dao
 * Created by HaiSheng on 2015/7/11.
 */
public class RanKingDao {

    /** 删除信息 */
    public static void deleteUserInfo(final FindCallback<AVObject> findCallback){
        //根据用户名查询，当前最高成绩
        final AVQuery<AVObject> queryUser = new AVQuery<AVObject>("Results");
        queryUser.whereEqualTo("name", SPUtils.getObject("user.name"));
        queryUser.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                for (AVObject av:list) {
                    av.deleteInBackground();
                }
                findCallback.done(list,e);
            }
        });
    }

    /** 提交结果到服务器 */
    public static void updateUserInfo(GameScenarioInfo gameScenarioInfo){
        AVObject resultsObj = new AVObject("Results");
        resultsObj.put("name",SPUtils.getObject("user.name"));
        resultsObj.put("nowResults",gameScenarioInfo.nowResults);
        resultsObj.put("nowScenarion",gameScenarioInfo.nowScenarion-1);
        resultsObj.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
            }
        });
    }





}
