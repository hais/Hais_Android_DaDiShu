package pw.hais.dadishu.app;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.avos.avoscloud.AVAnalytics;

import pw.hais.app.AppBaseActivity;

/**
 * 基础activity
 * Created by HaiSheng on 2015/7/4.
 */
public class BaseActivity extends AppBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //防止休眠
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //设置全屏
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //设置无导航栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        AVAnalytics.trackAppOpened(getIntent());

    }

    protected void onPause() {
        super.onPause();
        AVAnalytics.onFragmentEnd(tag);
    }

    protected void onResume() {
        /**
         * 设置为横屏
         */
        if (getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        super.onResume();
        AVAnalytics.onFragmentStart(tag);
    }

}
