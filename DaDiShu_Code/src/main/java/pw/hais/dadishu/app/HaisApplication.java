package pw.hais.dadishu.app;

import com.avos.avoscloud.AVAnalytics;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.PushService;

import pw.hais.app.AppApplication;
import pw.hais.dadishu.activitys.StartActivity;
import pw.hais.dadishu.helpers.AudioHelper;
import pw.hais.utils.SPUtils;

/**
 * App全局
 * Created by HaiSheng on 2015/7/4.
 */
public class HaisApplication extends AppApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        //初始化AV平台
        AVOSCloud.initialize(this, "bjzo7hiqwt9y6f3rdp30ynho41dusnfs1o69qwjw22qdm1n2", "w6gebanowo974sna63wklujtll74w9c91ban2la9az7dna6b");
        AVAnalytics.enableCrashReport(this, true);  //启动日记

        AudioHelper.initSound(getBaseContext());    //初始化声音

        if(SPUtils.getObject(StartActivity.SP_SHENGYIN_KEY) == null){
            SPUtils.saveObject(StartActivity.SP_SHENGYIN_KEY,true);
        }
    }
}
